require "json"
require "rmagick"
require "fileutils"

include Magick

FONT = "Roboto"

def text_fit?(text, width)
  tmp_image = Image.new(width, 500)
  drawing = Draw.new
  drawing.annotate(tmp_image, 0, 0, 0, 0, text) { |txt|
    txt.gravity = Magick::NorthGravity
    txt.pointsize = 29
    txt.fill = "#ffffff"
    txt.font = FONT
    txt.font_weight = Magick::BoldWeight
  }
  metrics = drawing.get_multiline_type_metrics(tmp_image, text)
  (metrics.width < width)
end

def fit_text(text, width)
  separator = ' '
  line = ''

  if not text_fit?(text, width) and text.include? separator
    i = 0
    text.split(separator).each do |word|
      if i == 0
        tmp_line = line + word
      else
        tmp_line = line + separator + word
      end

      if text_fit?(tmp_line, width)
        unless i == 0
          line += separator
        end
        line += word
      else
        unless i == 0
          line +=  "\n"
        end
        line += word
      end
      i += 1
    end
    text = line
  end
  return text
end

def add_grid(img, offset, color)
  width = img.columns
  height = img.rows

  grid = Draw.new
  grid.stroke = color

  (offset..width).step(offset) do |i|
    grid.line(i, 0, i, height)
  end

  (offset..height).step(offset) do |i|
    grid.line(0, i, width, i)
  end

  grid.draw(img)
end

BOLD_FONT = "fonts/Lato-Bold.ttf"
REGULAR_FONT = "fonts/Lato-Regular.ttf"
BLACK_FONT = "fonts/Lato-Black.ttf"

def generate(card)
  name = card["name"]

  text = "has successfully completed Elevated Brain Memory Power Workshop on #{card["date"]}"
  if card["class"] != "Personal"
    text = "has successfully completed Elevated Brain Memory Power Workshop at #{card["class"]} on #{card["date"]}"
  end

  referral_id = "Referral ID: #{card["referral_id"]}"

  img = Image.read("template.png").first
  width = img.columns
  height = img.rows
  
  name_img = Draw.new
  name_img.stroke("transparent")
  name_img.gravity = CenterGravity
  name_img.annotate(img, width, 28, 0, 680, fit_text(name, width)) { |txt|
    txt.font = BOLD_FONT
    txt.pointsize = 70
    txt.kerning = 2
    txt.fill = "#7287c3"
  }

  name_img.draw(img)

  text_img = Draw.new
  text_img.stroke("transparent")
  text_img.gravity = NorthGravity
  text_img.annotate(img, width, 28, 0, 790, fit_text(text, 980)) { |txt|
    txt.font = REGULAR_FONT
    txt.pointsize = 40
    txt.kerning = 2
    txt.fill = "#000000"
  }
  text_img.draw(img)

  referral = Draw.new
  referral.stroke("transparent")
  referral.gravity = CenterGravity
  referral.annotate(img, width, 28, 0, 950, fit_text(referral_id, width)) { |txt|
    txt.font = BOLD_FONT
    txt.pointsize = 40
    txt.kerning = 2
    txt.fill = "#7287c3"
    txt.font_weight = Magick::BoldWeight
  }

  referral.draw(img)

  # add_grid(img, 10, "#FFFF00")
  # add_grid(img, 100, "#00FFFF")
  # img.display
  filename = "output/#{card["referral_id"]}.png"
  print filename, "\n"
  img.write(filename)
end

def main()
  file = File.open "./parsed.json"
  cards = JSON.load file

  for card in cards
    generate(card)
  end
end

main()
