#!/bin/bash

for filename in output/*.png; do
    base=$(basename -- $filename)
    convert $filename postprocessing/"${base%.*}.pdf" &
done

