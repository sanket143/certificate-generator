const csv = require('csv-parser')
const fs = require('fs')
const results = [];

fs.createReadStream('data.csv')
  .pipe(csv())
  .on('data', (data) => {
    results.push({
      name: data.name.replace(/^\s+|\s+$/gmi, ""),
      class: data.class.replace(/^\s+|\s+$/gmi, ""),
      date: data.date,
      referral_id: data.referral_id
    })
  })
  .on('end', () => {
    console.log(JSON.stringify(results));
    // [
    //   { NAME: 'Daffy Duck', AGE: '24' },
    //   { NAME: 'Bugs Bunny', AGE: '22' }
    // ]
  });
